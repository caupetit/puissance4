/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   end_prog.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: po-conno <po-conno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/08 00:07:10 by po-conno          #+#    #+#             */
/*   Updated: 2014/03/09 22:43:06 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"
#include "board.h"

void		tab_free(t_board *board, char **tab)
{
	int		i;

	if (!tab)
		return ;
	i = -1;
	while (++i < Y)
		ft_strdel(&tab[i]);
	free(tab);
}

int			clean_prog(t_board *board)
{
	tab_free(board, board->tab);
	tab_free(board, board->cpy.tab);
	return (0);
}
