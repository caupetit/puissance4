/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: po-conno <po-conno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/08 17:24:33 by po-conno          #+#    #+#             */
/*   Updated: 2014/03/09 22:27:54 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include "board.h"
#include "libft.h"

static int	check_buffer(t_board *board, char *buffer)
{
	int		i;

	i = 0;
	while (buffer[i])
	{
		if (!ft_isdigit(buffer[i++]))
			return (ft_error(_argv_type));
	}
	if (board->last >= board->width)
		return (ft_error(_wrong_val));
	if (board->last < 0)
		return (-1);
	return (1);
}

static int	leave_game(char **line, int error)
{
	ft_strdel(line);
	if (error == -1)
		return (0);
	if (error == -2)
		return (-1);
	return (ft_error(error));
}

int			add_token(t_board *board)
{
	char		*line;
	int			ret;

	line = NULL;
	ft_putstr("Enter a row number : ");
	if (board->player)
	{
		if ((ret = ft_get_next_line(0, &line)) == -1)
			return (ft_error(_errno));
		if (!ret)
			return (leave_game(&line, -1));
		if (line)
			board->last = (ft_atoi(line) - 1);
		if (check_buffer(board, line) < 0)
			return (leave_game(&line, -2));
	}
	else
		board->last = ia(board);
	ft_putchar('\n');
	if (modify_board(board, board->last, board->player) == -1)
		return (leave_game(&line, -2));
	ft_strdel(&line);
	return (1);
}

int			first_player(void)
{
	int		ret;

	srand(time(NULL));
	ret = rand();
	if (ret % 2 == 0)
		return (0);
	return (1);
}
