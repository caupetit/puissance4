/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   board.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: po-conno <po-conno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/07 21:30:45 by po-conno          #+#    #+#             */
/*   Updated: 2014/03/09 22:53:03 by po-conno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "board.h"
#include "libft.h"

void		board_cpy(t_board *board)
{
	int		i;
	int		j;

	board->cpy.last = board->last;
	board->cpy.turn_nb = board->turn_nb;
	i = -1;
	while (++i < Y)
	{
		j = -1;
		while (++j < X)
			board->cpy.tab[i][j] = board->tab[i][j];
	}
}

static int	get_size(char *size)
{
	int		i;

	i = 0;
	while (size[i])
	{
		if (!ft_isdigit(size[i]))
			return (ft_error(1));
		++i;
	}
	return (ft_atoi(size));
}

int			modify_board(t_board *board, int x, int player)
{
	char	token;
	int		i;

	i = 0;
	if (board->tab[i][board->last] != '.')
		return (ft_error(_row_full));
	while (i < board->height && board->tab[i][board->last] == '.')
		++i;
	token = player == CPU ? CPU_C : PLAYER_C;
	board->tab[i - 1][x] = token;
	board->turn_nb += 1;
	return (0);
}

int			create_board(t_board *board, char *height, char *width)
{
	int		i;
	int		j;

	i = 0;
	if ((Y = get_size(height)) <= 0 || (X = get_size(width)) <= 0)
		return (ft_error(_usage));
	if (X < MIN_X || X > MAX_SIZE || Y < MIN_Y || Y > MAX_SIZE)
		return (ft_error(_argv_size));
	if ((board->tab = (char **)malloc(sizeof(char *) * Y)) == NULL)
		return (ft_error(_errno));
	if ((board->cpy.tab = (char **)malloc(sizeof(char *) * Y)) == NULL)
		return (ft_error(_errno));
	while (i < board->height)
	{
		j = 0;
		if ((board->tab[i] = ft_strnew(board->width)) == NULL)
			return (ft_error(_errno));
		if ((board->cpy.tab[i] = ft_strnew(board->width)) == NULL)
			return (ft_error(_errno));
		while (j < board->width)
			board->tab[i][j++] = '.';
		++i;
	}
	print_board(board);
	return (0);
}

void		init_board(t_board *board)
{
	board->height = 0;
	board->width = 0;
	board->last = -1;
	board->player = first_player();
	board->turn_nb = 0;
	board->cpy.last = 0;
	board->cpy.turn_nb = 0;
	board->tab = NULL;
	board->cpy.tab = NULL;
}
