/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ia.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: caupetit <caupetit@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/09 19:46:16 by caupetit          #+#    #+#             */
/*   Updated: 2014/03/09 22:43:39 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"
#include "board.h"

int			modify_cpy(t_board *board, int x, char c)
{
	char	token;
	int		i;

	i = 0;
	if (board->cpy.tab[0][board->cpy.last] != '.')
		return (-1);
	while (i < Y && board->cpy.tab[i][board->cpy.last] == '.')
		++i;
	token = c;
	board->cpy.tab[i - 1][x] = token;
	board->cpy.turn_nb += 1;
	return (0);
}

static int	ia_win(t_board *board, char c)
{
	int		i;

	i = -1;
	while (++i < X)
	{
		board_cpy(board);
		board->cpy.last = i;
		if (modify_cpy(board, i, c) != -1)
		{
			if (check_winner(board, AI))
				return (i);
		}
	}
	return (-1);
}

static void	get_alignment(t_board *board, t_ia *ia, int j)
{
	t_fu	fu[4] =
	{
		check_vertical,
		check_right_diagonal,
		check_left_diagonal,
		check_horizontal
	};
	int		i;
	int		k;

	i = -1;
	k = 0;
	while (k < Y && board->cpy.tab[k][board->cpy.last] == '.')
		k++;
	while (++i < 4)
	{
		ia->ret = fu[i](board, board->cpy.tab, j, k);
		if (ia->ret > ia->best)
		{
			ia->best = ia->ret;
			ia->col = j;
		}
	}
}

static int	get_best_alignment(t_board *board, char c)
{
	int		j;
	t_ia	ia;

	j = -1;
	ia.best = 0;
	ia.col = 0;
	ia.ret = 0;
	while (++j < X)
	{
		board_cpy(board);
		board->cpy.last = j;
		if (modify_cpy(board, j, c) != -1)
			get_alignment(board, &ia, j);
	}
	return (ia.col);
}

int			ia(t_board *board)
{
	int		ret;
	int		ret1;

	if (board->turn_nb == 0)
		return (X / 2);
	if (board->turn_nb == 1 && board->last != X - 1)
		return (board->last + 1);
	if (board->turn_nb == 1 && board->last != 0)
		return (board->last - 1);
	if ((ret = ia_win(board, CPU_C)) != -1)
		return (ret);
	if ((ret = ia_win(board, PLAYER_C)) != -1)
		return (ret);
	ret = get_best_alignment(board, PLAYER_C);
	ret1 = get_best_alignment(board, CPU_C);
	if (ret > ret1)
		return (ret);
	return (ret1);
}
