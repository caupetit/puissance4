/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: po-conno <po-conno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/08 18:05:19 by po-conno          #+#    #+#             */
/*   Updated: 2014/03/09 22:44:35 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "board.h"
#include "libft.h"

void		print_end_game(t_board *board)
{
	if (board->turn_nb == board->width * board->height)
		ft_putstr("IT'S A DRAW !\n");
	if (check_winner(board, PLAYER) && !board->player)
		ft_putstr("YOU WIN !\n");
	if (check_winner(board, PLAYER) && board->player)
		ft_putstr("CPU WINS !\n");
}

void		print_player(t_board *board, int i, int j)
{
	if (board->tab[i][j] == PLAYER_C)
		ft_printf("\033[1;31m%c\033[0;m", (board->tab)[i][j]);
	else
		ft_printf("\033[1;33m%c\033[0;m", (board->tab)[i][j]);
}

int			print_board(t_board *board)
{
	int		i;
	int		j;

	write(1, "\033[2J\033[r", 8);
	i = 0;
	while (i < Y)
	{
		j = 0;
		while (j < X)
		{
			if (j)
				ft_putchar(' ');
			if (board->tab[i][j] != '.')
				print_player(board, i, j++);
			else
				ft_putchar(board->tab[i][j++]);
		}
		ft_putchar('\n');
		++i;
	}
	print_col(X);
	return (1);
}

void		print_col(int x)
{
	int		i;
	int		col;

	col = 0;
	i = 1;
	while (i <= x)
	{
		if (i != 1)
		ft_putchar(' ');
		if (i % 10 == 0)
			col = !col;
		if (col)
			ft_printf("\033[0;37m%d\033[0;m", i % 10);
		else
			ft_printf("\033[1;34m%d\033[0;m", i % 10);
		++i;
	}
	ft_putchar('\n');
}
