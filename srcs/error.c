/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: po-conno <po-conno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/07 20:29:58 by po-conno          #+#    #+#             */
/*   Updated: 2014/03/08 22:03:08 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "board.h"
#include "libft.h"
#include <errno.h>

int		ft_error(int err)
{
	char	*errors[] = {
		"Too many arguments.\n",
		"Enter a positive numeric values.\n",
		"Min height : 6, min width : 7.\n",
		"Wrong value.\n",
		"This row is full, choose another one.\n",
		"Usgae : cmd height width.\n"
	};

	if (err == _errno)
		ft_puterror(strerror(errno));
	else
		ft_puterror(errors[err]);
	return (-1);
}
