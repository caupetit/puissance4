/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_winner.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: po-conno <po-conno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/08 00:46:43 by po-conno          #+#    #+#             */
/*   Updated: 2014/03/09 21:28:40 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "board.h"
#include "libft.h"

int		check_vertical(t_board *board, char **tab, int x, int y)
{
	int		i;
	char	token;

	i = 0;
	if (y < Y)
		token = tab[y][x];
	while (y < Y && tab[y][x] == token)
	{
		++y;
		++i;
	}
	return (i);
}

int		check_horizontal(t_board *board, char **tab, int x, int y)
{
	int		i;
	char	token;

	token = tab[y][x];
	i = 0;
	while (x && tab[y][x] == token)
		--x;
	if (tab[y][x] != token)
		++x;
	while (x < X && tab[y][x] == token)
	{
		++x;
		++i;
	}
	return (i);
}

int		check_left_diagonal(t_board *board, char **tab, int x, int y)
{
	int		i;
	char	token;

	i = 0;
	token = tab[y][x];
	while (x && y && tab[y][x] == token)
	{
		--x;
		--y;
	}
	if (tab[y][x] != token)
	{
		++y;
		++x;
	}
	while (x < X && y < Y && tab[y][x] == token)
	{
		++y;
		++x;
		++i;
	}
	return (i);
}

int		check_right_diagonal(t_board *board, char **tab, int x, int y)
{
	int		i;
	char	token;

	i = 0;
	token = tab[y][x];
	while (x && y < Y && tab[y][x] == token)
	{
		--x;
		++y;
	}
	if (y == Y || tab[y][x] != token)
	{
		--y;
		++x;
	}
	while (x < X && y && tab[y][x] == token)
	{
		--y;
		++x;
		++i;
	}
	return (i);
}

int		check_winner(t_board *board, int player)
{
	int		i;
	int		last;
	char	**tab;

	i = 0;
	tab = player ? board->tab : board->cpy.tab;
	last = player ? board->last : board->cpy.last;
	while (i < Y && tab[i][last] == '.')
		++i;
	if (check_vertical(board, tab, last, i) >= 4)
		return (1);
	if (check_horizontal(board, tab, last, i) >= 4)
		return (1);
	if (check_left_diagonal(board, tab, last, i) >= 4)
		return (1);
	if (check_right_diagonal(board, tab, last, i) >= 4)
		return (1);
	return (0);
}
