/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_puterror.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: po-conno <po-conno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/26 15:32:35 by po-conno          #+#    #+#             */
/*   Updated: 2014/01/12 14:14:08 by po-conno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "libft.h"

void	ft_puterror(char const *s)
{
	int		i;

	i = 0;
	if (!s)
		return ;
	write(2, s, ft_strlen(s));
}
