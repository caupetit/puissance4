/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   board.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: po-conno <po-conno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/09 22:56:53 by po-conno          #+#    #+#             */
/*   Updated: 2014/03/09 22:56:54 by po-conno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BOARD_H
# define BOARD_H

# define CPU		0
# define PLAYER		1
# define AI			0
# define MAX_SIZE	1000
# define MIN_X		7
# define MIN_Y		6
# define X			board->width
# define Y			board->height
# define PLAYER_C	'X'
# define CPU_C		'O'

enum					e_enum
{
	_argv_nb,
	_argv_type,
	_argv_size,
	_wrong_val,
	_row_full,
	_usage,
	_errno
};

typedef struct			s_ia
{
	int					col;
	int					best;
	int					ret;
}						t_ia;

typedef struct			s_cpy
{
	char				**tab;
	int					last;
	int					turn_nb;
}						t_cpy;

typedef struct			s_board
{
	int					height;
	int					width;
	int					last;
	int					player;
	int					turn_nb;
	char				**tab;
	t_cpy				cpy;
}						t_board;

typedef int		(*t_fu)(t_board *, char **, int, int);

/*
** board.c (1 static)
*/
int		create_board(t_board *board, char *height, char *width);
int		modify_board(t_board *board, int x, int player);
int		print_board(t_board *board);
void	board_cpy(t_board *board);

/*
**		ia.c (3 static)
*/
int		modify_cpy(t_board *board, int x, char c);
int		ia(t_board *board);
/*
** check_winner.c (4 static)
*/
int		check_right_diagonal(t_board *board, char **tab, int x, int y);
int		check_left_diagonal(t_board *board, char **tab, int x, int y);
int		check_horizontal(t_board *board, char **tab, int x, int y);
int		check_vertical(t_board *board, char **tab, int x, int y);
int		check_winner(t_board *board, int player);

/*
** game.c (2 static)
*/
int		add_token(t_board *board);
int		first_player(void);

/*
** print.c
*/
void	init_board(t_board *board);
void	print_end_game(t_board *board);
void	print_col(int x);

/*
** end_prog.c
*/
int		clean_prog(t_board *board);

/*
** error.c
*/
int		ft_error(int err);

#endif /* !ALCU_H */
