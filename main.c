/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: po-conno <po-conno@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/07 20:26:42 by po-conno          #+#    #+#             */
/*   Updated: 2014/03/09 22:42:17 by caupetit         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "libft.h"
#include "board.h"

int		main(int argc, char **argv)
{
	t_board		board;
	int			end;
	int			ret;

	if (argc != 3)
		return (ft_error(_usage));
	end = 0;
	init_board(&board);
	if (create_board(&board, argv[1], argv[2]) == -1)
		return (clean_prog(&board));
	while (!end)
	{
		while ((ret = add_token(&board)) == -1)
			;
		print_board(&board);
		if (!ret || board.turn_nb == board.width * board.height)
			break ;
		end = check_winner(&board, PLAYER);
		board.player = !board.player;
	}
	print_end_game(&board);
	clean_prog(&board);
	return (0);
}
