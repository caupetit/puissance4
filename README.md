# README #

* * *

Puissance4 is a 48h project implementing a simple version of Connect 4 game.

Works on linux and mac.

* * *

# How to use ? #

Simply copy the following commands:

* make 

* ./puissance4 <height> <width>

Enter your column number to play

* * *

# Screenshot #

![Puissance4.jpg](https://bitbucket.org/repo/8Ard4n/images/3783136539-Puissance4.jpg)