#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: po-conno <po-conno@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/03/07 22:38:51 by po-conno          #+#    #+#              #
#    Updated: 2014/03/09 19:57:46 by caupetit         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME = puissance4

HEADERS =	-I headers/ \
			-I libft/includes/

LIBFT =	libft/libft.a

SRC =	main.c \
		srcs/error.c \
		srcs/board.c \
		srcs/init_struct.c \
		srcs/check_winner.c \
		srcs/game.c \
		srcs/print.c \
		srcs/end_prog.c \
		srcs/ia.c

OBJ = $(SRC:.c=.o)

CC = gcc -g -Wall -Wextra -Werror

%.o: %.c
	$(CC) -o $@ -c $< $(HEADERS)

all: $(NAME)

$(NAME): $(OBJ)
	make -C libft/
	$(CC) -o $@ $(OBJ) $(NAMELIB) $(LIBFT)

clean:
	@make clean -C libft/
	@rm -f $(OBJ)

fclean:
	@make fclean -C libft/
	@rm -f $(OBJ)
	@rm -f $(NAMELIB)
	@rm -f $(NAME)

re: fclean all
